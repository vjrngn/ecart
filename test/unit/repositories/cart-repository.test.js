"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/* eslint-disable no-undef */
const cart_repository_1 = require("../../../src/repositories/cart-repository");
const foo = [
    {
        id: 1,
        total: 0,
        subtotal: 0,
        taxTotal: 0,
        discountPercentage: 0,
        items: [
            {
                id: 1,
                name: 'one',
                price: 2,
                quantity: 1,
                cart_id: 1,
                cartId: 1
            },
            {
                id: 2,
                name: 'two',
                price: 1,
                quantity: 1,
                cart_id: 1,
                cartId: 1
            }
        ]
    }
];
describe('CartRepository', () => {
    const cartTableRows = [
        {
            id: 1,
            total: '0.00',
            subtotal: '0.00',
            tax_total: '0.00',
            discount_percentage: '0.00'
        }
    ];
    const cartItemRows = [
        { id: 1, name: 'one', price: '2.00', quantity: 1, cart_id: 1 },
        { id: 2, name: 'two', price: '1.00', quantity: 1, cart_id: 1 }
    ];
    test('should map cart rows from raw DB representation', () => {
        const result = cart_repository_1.transformCartRows(cartTableRows);
        expect(result).toEqual([
            {
                id: 1,
                total: 0,
                subtotal: 0,
                taxTotal: 0,
                discountPercentage: 0,
                items: []
            }
        ]);
    });
    test('should map cart item rows from raw DB representation', () => {
        const result = cart_repository_1.transformCartItems(cartItemRows);
        expect(result).toEqual([
            {
                id: 1,
                name: 'one',
                price: 2,
                quantity: 1,
                cart_id: 1,
                cartId: 1
            },
            {
                id: 2,
                name: 'two',
                price: 1,
                quantity: 1,
                cart_id: 1,
                cartId: 1
            }
        ]);
    });
});
//# sourceMappingURL=cart-repository.test.js.map