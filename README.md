### ecart


#### Database schema
This repo uses an interactive way of communicating ERD using [dbdiagram](https://dbdiagram.io/d/620f43d0485e433543d56b64)

#### Clone
Clone this repository with
```sh
git clone git@gitlab.com:vjrngn/ecart.git
```

#### Setup
Run the following command to setup the project. This command ensures git hygene.
```sh
  cd setup
  sh hooks.sh
```

##### Migrations
This project uses migrations to handle updates to the database - creation of tables,
updating existing tables or dropping tables.

Migrations allow us to version control changes to the database and don't require us
to write SQL scripts manually.

If you would like to read more, check out the [knex migrations](https://knexjs.org/#Installation-migrations) doc page.

##### Docker
For local development and testing, we use docker and docker compose. Make sure you
have docker installed. You can find documentation for your OS [here](https://docs.docker.com/get-docker/)

#### Environment
This repository relies on the node process environment for its' configuration.
For ease, we use the `dotenv` library.

Do the following to get the app ready to run :

1. Run `cp .env.example` `.env` to make a copy.
2. Run `docker compose up -d`. This brings up the relevant docker containers defined in `docker-compose.yml`
   1. Make sure you have the database created. To do so, run the following
      1. `docker compose exec database /bin/bash`
      2. `psql -U postgres`
      3. `CREATE DATABASE ecart`
3. Run `npm run start:dev` to bring up the local server


#### Testing
Run all tests with
```sh
  npm run test # or npm t
```

### Usage
The service exposes only one endpoint - `GET https://safe-atoll-23821.herokuapp.com/carts` that returns a list of cart information along with the items in them.

```sh
curl -X https://safe-atoll-23821.herokuapp.com/carts | jq
```

Sample Response Headers
```http
HTTP/1.1 200 OK
X-Powered-By: Express
Content-Type: application/json; charset=utf-8
Content-Length: 224
ETag: W/"e0-d/Sqy/Tv0AhIVVAO5gUtI85KeJk"
Date: Fri, 18 Feb 2022 13:31:32 GMT
Connection: keep-alive
Keep-Alive: timeout=5
```

Sample Response Body
```json
{
  "data": [
    {
      "id": 1,
      "total": 0,
      "subtotal": 0,
      "taxTotal": 0,
      "discountPercentage": 0,
      "items": [
        {
          "id": 1,
          "name": "one",
          "price": 2,
          "quantity": 1,
          "cart_id": 1,
          "cartId": 1
        },
        {
          "id": 2,
          "name": "two",
          "price": 1,
          "quantity": 1,
          "cart_id": 1,
          "cartId": 1
        }
      ]
    }
  ]
}
```

#### Deployment
This app can be deployed to Heroku as-is. Follow the instructions [here](https://devcenter.heroku.com/articles/deploying-nodejs#prerequisites)
to get the app deployed.

[Click Here](https://safe-atoll-23821.herokuapp.com/) to visit the app.

App URL: `https://safe-atoll-23821.herokuapp.com/`.

#### Considerations, Thoughts and Design Decisions

- Config - In real world scenarios, I would consider different means on providing application configuration to the deployed app
    - Use parameter store, inject variable during docker image creation
    - Use something like vault and allow the application to query vault at app boot
- The database needs to be provisioned beforehand. Can use Terraform or ansible to automate this process and have a centralised infra repository that handles all infrastructure requirements. Since we are hosting on Heroku, this is not currently something we need to worry about.
- I would change the the schema of the database outlined in the assignment. Aggregations don’t necessarily need to be stored especially for this application as the cart is likely going to be updated multiple times. Since all the data is present to compute the aggregation, it’s a lot more efficient to do them on the read query of cart items rather than store them every time there’s an update.
    - Discount information can be maintained per row (this is domain specific) but if the use-case allowed or wanted to allow flexibility for discounting per product (cart item) this design would be better.
    - Each cart item can also have cost information per row instead of maintaining a running subtotal, tax and total. Typically cart items are a representation of products (ie, inventory) so referencing the product itself from the `cart_items` table would be beneficial (for a number of reasons). Pricing, discounting etc can be moved away from the “domain” of the cart into where it belongs - closer to the product, giving business teams more control.
- Usage of knex - Given the problem statement required minimal use of frameworks, I chose to use `knex` as the minimum abstraction for interactions with the database. This library is merely a sql builder and does not have the “bells and whistles” of a full fledged ORM like TypeORM or Bookshelf. In a real world project, I would use an ORM as it reduces the time required (testing + implementation) to get common queries (one to one, on to many, many to many) out of the way; allowing us to be more productivity. Ex: Mapping between camel case and snake case is a manual task.
- Considerations made but did not implement
    - Logging - while the logger has been configured, the solution does not have logging as done in production applications due to time constraints.
    - CI/CD with GitLab. Typically my workflow entails setting up a CI pipeline for PRs and a manual step for deployment (CD).
