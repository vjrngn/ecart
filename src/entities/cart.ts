import { CartItem } from './cart-item';

export type Cart = {
  id: number;
  total: number;
  subtotal: number;
  taxTotal: number;
  discountPercentage: number;
  items?: CartItem[];
}
