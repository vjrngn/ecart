export type CartItem = {
  cart_id: number;
  name: string;
  price: number;
  quantity: number;
}
