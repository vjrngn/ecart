import { Knex } from 'knex';
import { Cart } from '../entities/cart';
import { CartItem } from '../entities/cart-item';

/**
 * Transform raw cart rows into the shape the application
 * expects it.
 *
 * @export
 * @param {*} rows
 * @returns {Cart[]}
 */
export function transformCartRows(rows: any): Cart[] {
  return rows.map((row: any) => {
    return {
      id: row.id,
      total: parseFloat(row.total),
      subtotal: parseFloat(row.subtotal),
      taxTotal: parseFloat(row.tax_total),
      discountPercentage: parseFloat(row.discount_percentage),
      items: []
    };
  });
}

/**
 * Transform raw cart item rows into the shape the application
 * expects it.
 *
 * @export
 * @param {*} rows
 * @returns {CartItem[]}
 */
export function transformCartItems(rows: any): CartItem[] {
  return rows.map((row: any) => {
    return {
      ...row,
      price: parseFloat(row.price),
      cartId: row.cart_id
    };
  });
}

/**
 * This class is responsible for interacting with the 'carts' domain.
 * It abstracts the connection and transformation logic from other
 * layers of the application.
 *
 * @export
 * @class CartRepository
 */
export class CartRepository {
  private database;

  constructor(database: Knex) {
    this.database = database;
  }

  /**
   * Fetch all cart data with items from the database
   *
   * @returns {Promise<Cart[]>}
   * @memberof CartRepository
   */
  async getAllCarts(): Promise<Cart[]> {
    const cartRows = await this.database.from('cart')
      .select('*')
      .then(transformCartRows);

    const cartIds = cartRows.map((row: Cart) => row.id);
    const cartItems = await this.database.from('cart_items')
      .select('*')
      .where('cart_id', 'in', cartIds)
      .then(transformCartItems);

    return cartRows.map(cartRow => {
      const items = cartItems.filter(cartItem => cartItem.cart_id === cartRow.id);
      return Object.assign({}, cartRow, { items });
    }) as Cart[];
  }
}
