import express, { Request, Response } from 'express';
import createError from 'http-errors';
import { ApplicationConfiguration } from './config/config';
import { connect } from './database';
import { CartRepository } from './repositories/cart-repository';

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

export async function initApp(config: ApplicationConfiguration) {
  const connection = await connect(config);

  const repo = new CartRepository(connection);

  app.get('/carts', (_: Request, res: Response) => {
    repo.getAllCarts()
      .then((cartData) => {
        return res.json({
          data: cartData
        });
      });
  });

  app.use(function (_, __, next) {
    next(createError(404));
  });

  return app;
};
