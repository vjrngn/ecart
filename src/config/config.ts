require('dotenv').config();

const { APP_ENV, MIGRATE_ON_START, DB_HOST, DATABASE_URL, DB_PORT, DB_USER, DB_PASSWORD, DB_NAME } =
  process.env;

export interface ApplicationConfiguration {
  connectionURL?: string;
  database: {
    host: string | undefined;
    port: number | undefined;
    username: string | undefined;
    password: string | undefined;
    name: string | undefined;
    schema: string;
  };
  environment?: string;
  migrateOnStart: boolean;
}

export const config: ApplicationConfiguration = {
  database: {
    host: DB_HOST,
    port: parseInt(DB_PORT!),
    username: DB_USER,
    password: DB_PASSWORD,
    name: DB_NAME,
    schema: 'public'
  },
  environment: APP_ENV,
  connectionURL: DATABASE_URL,
  migrateOnStart: MIGRATE_ON_START === 'true'
};
