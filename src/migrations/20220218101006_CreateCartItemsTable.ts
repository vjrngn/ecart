const FOREIGN_KEY_CART_ID = 'fk_cart_id';

exports.up = function (knex: any) {
  return knex.schema.createTable('cart_items', (table: any) => {
    table.increments('id');
    table.string('name');
    table.decimal('price', 4);
    table.smallint('quantity');

    table.integer('cart_id').unsigned().notNullable();
    table
      .foreign('cart_id')
      .references('cart.id')
      .withKeyName('fk_cart_id');
  });
};

exports.down = function (knex: any) {
  return knex.schema.dropTable('cart_items', (table: any) => {
    table.dropForeign(FOREIGN_KEY_CART_ID);
  });
};
