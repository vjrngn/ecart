exports.up = function (knex: any) {
  return knex.schema.createTable('cart', (table: any) => {
    table.increments('id');
    table.decimal('total', 4);
    table.decimal('subtotal', 4);
    table.decimal('tax_total', 4);
    table.decimal('discount_percentage', 2);
  });
};

exports.down = function (knex: any) {
  return knex.schema.dropTable('cart');
};
