exports.seed = async function(knex: any) {
  // Deletes ALL existing entries
  await knex('cart_items').insert([
    {
      cart_id: 1,
      name: 'Shoes',
      price: 24.75,
      quantity: 1
    },
    {
      cart_id: 1,
      name: 'Sun Glasses',
      price: 24.75,
      quantity: 1
    },
    {
      cart_id: 2,
      name: 'Ear Phones',
      price: 24.75,
      quantity: 1
    },
    {
      cart_id: 2,
      name: 'Trousers',
      price: 24.75,
      quantity: 1
    }
  ]);
};
