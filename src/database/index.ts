import { ApplicationConfiguration } from '../config/config';
import { join } from 'path';
import isEmpty from 'lodash.isempty';
const knex = require('knex');

export function connect(config: ApplicationConfiguration): Promise<any> {
  console.log(config);

  const {
    database: { host, port, username, password, name },
    environment
  } = config;
  return new Promise((resolve, reject) => {
    const connectionString = !isEmpty(config.connectionURL)
      ? config.connectionURL
      : `postgres://${username}:${password}@${host}:${port}/${name}`;

    const connectionConfig = {
      client: 'pg',
      connection: {
        connectionString
      },
      migrations: {
        tableName: 'migrations',
        directory: join(__dirname, '..', 'migrations')
      },
      seeds: {
        directory: join(__dirname, '..', 'seeds')
      }
    };

    if (environment && environment !== 'dev') {
      // @ts-ignore
      connectionConfig.connection.ssl = {
        rejectUnauthorized: false
      };
    }

    console.log('Connected to database with config \n', JSON.stringify(connectionConfig));

    const connection = knex(connectionConfig);

    if (config.migrateOnStart) {
      console.log('Running migrations');
      connection.migrate.latest().then(() => {
        return connection.seed.run();
      }).then(() => {
        console.log('Migrations run successfully');
        resolve(connection);
      }).catch(reject);
    } else {
      resolve(connection);
    }
  });
}
