require('dotenv').config();

const {
  DB_HOST,
  DB_PORT,
  DB_USER,
  DB_PASSWORD,
  DB_NAME
} = process.env;

module.exports = {
  client: 'pg',
  connection: {
    host: DB_HOST,
    port: DB_PORT,
    user: DB_USER,
    password: DB_PASSWORD,
    database: DB_NAME
  },
  migrations: {
    tableName: 'migrations',
    directory: 'src/migrations',
    loadExtenions: ['.ts'],
    extention: 'ts'
  },
  seeds: {
    directory: 'src/seeds',
    loadExtenions: ['.ts'],
    extention: 'ts'
  }
};
